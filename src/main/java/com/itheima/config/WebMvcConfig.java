package com.itheima.config;

import com.itheima.common.JacksonObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import java.util.List;

/**
 * 配置类：配置静态资源访问
 */
@Configuration
@Slf4j
public class WebMvcConfig extends WebMvcConfigurationSupport {
    //WebMvcConfigurationSupport:默认访问static目录下静态资源
    //重写配置 访问backend front目录下静态资源
    //1.从/backend/** 发起的请求 访问classpath下backend下资源
    //2./front/** 发起的请求 访问classpath下front下资源
    @Override
    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
        log.debug("************开始加载自定义静态资源*******************");
        registry.addResourceHandler("/backend/**").addResourceLocations("classpath:/backend/");
        registry.addResourceHandler("/front/**").addResourceLocations("classpath:/front/");
        log.debug("************结束加载自定义静态资源*******************");
    }


    /**
     * 注册自定义消息转换器
     */
    @Override
    protected void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
        //创建消息转换器对象
        MappingJackson2HttpMessageConverter messageConverter  =new MappingJackson2HttpMessageConverter();
        //将自定义消息转换器对象设置到转换器中
        messageConverter.setObjectMapper(new JacksonObjectMapper());
        //将消息转换器对象 放到原有集合中 默认下标
        converters.add(7,messageConverter);
        log.debug("注册自定义消息转换器成功了");
    }
}
