package com.itheima.dto;


import com.itheima.entity.Dish;
import com.itheima.entity.DishFlavor;
import lombok.Data;
import java.util.ArrayList;
import java.util.List;

/**
 * dto:DTO，全称为Data Transfer Object，即数据传输对象，一般用于展示层与服务层之间的数据传输。
 * 1.接收页面数据（新增菜品）
 * 2.响应数据给页面（分页查询）
 */
@Data
public class DishDto extends Dish {

    //菜品口味
    private List<DishFlavor> flavors = new ArrayList<>();

    //分类名称
    private String categoryName;

    private Integer copies;
}
