package com.itheima.controller;

import com.itheima.common.PageResult;
import com.itheima.common.R;
import com.itheima.entity.Employee;
import com.itheima.service.EmployeeService;
import com.itheima.utils.BaseContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * 员工管理控制层
 * <p>
 * R:Result对象 返回给前端页面
 * CRUD:
 * code  1：成功 0：失败
 * msg   新增成功了  新增失败了    删除成功了  删除失败了   修改成功了  修改失败了    查询成功了 查询失败了
 * data  null                                                             Employee    List<Emploee>
 */
@RestController
@RequestMapping("/employee")
@Slf4j
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    /**
     * 后台登录功能
     */
    //@RequestMapping(value = "/login",method = RequestMethod.POST)
    @PostMapping("/login")
    public R<Employee> login(@RequestBody Employee employee, HttpSession httpSession) {
        try {
            //登录业务处理 希望返回R结果对象
            return employeeService.login(employee, httpSession);
        } catch (Exception exception) {
            log.debug("******登录失败了******");
            return R.error("登录失败了");
        }
    }

    /**
     * 退出
     */
    @PostMapping("/logout")
    public R<String> logout(HttpSession httpSession) {
        try {
            employeeService.logout(httpSession);
            return R.error("退出成功了");//control+shift 上下键
        } catch (Exception exception) {
            exception.printStackTrace();
            return R.error("退出失败了");
        }
    }

    /**
     * 新增员工
     * employee: 页面表单中数据
     * httpSession: 获取谁新增员工的用户
     */
    @PostMapping
    //public R<String> save(@RequestBody Employee employee, HttpSession httpSession) {
    public R<String> save(@RequestBody Employee employee) {
        long id = Thread.currentThread().getId();
        log.debug("**LoginCheckFilter**线程id{}**",id);
        /*Employee ey = (Employee) httpSession.getAttribute("employee");
        int row = employeeService.save(employee, ey.getId());*/
        int row = employeeService.save(employee, BaseContext.getCurrentId());
        if (row == 1) {
            return R.success(null, "新增成功了");
        } else {
            return R.error("新增失败了");
        }
    }

    /**
     * 员工分页
     * R<返回对象>:可以返回Page 但不推荐 返回数据比较多，页面只需要两个records total
     * 所以采用自定义PageResult对象 只封装两个属性
     */
    @GetMapping("/page")
    public R<PageResult> findPage(@RequestParam(value = "page", defaultValue = "1") Long page,
                                  @RequestParam(value = "pageSize", defaultValue = "10") Long pageSize, String name) {
        PageResult<Employee> pageResult = employeeService.findPage(page,pageSize,name);
        return R.success(pageResult);
    }

    /**
     * 更新员工（不仅限于状态更新 员工启用 禁用）
     */
    @PutMapping
    public R<String> update(@RequestBody Employee employee){
        long id = Thread.currentThread().getId();
        log.debug("**LoginCheckFilter**线程id{}**",id);
        int row = employeeService.update(employee,BaseContext.getCurrentId());
        if (row == 1) {
            return R.success(null, "修改成功了");
        } else {
            return R.error("修改失败了");
        }
    }

    /**
     * 根据员工id查询员工信息
     */
    @GetMapping("/{id}")
    public R<Employee> findById(@PathVariable("id") Long id){
        Employee employee = employeeService.findById(id);
        return R.success(employee);
    }
}
