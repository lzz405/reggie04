package com.itheima.controller;

import com.itheima.common.PageResult;
import com.itheima.common.R;
import com.itheima.entity.Category;
import com.itheima.entity.Employee;
import com.itheima.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 分类管理控制层
 */
@RestController
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    /**
     * 新增分类（菜品、套餐）
     */
    @PostMapping
    public R<String> save(@RequestBody Category category){
        categoryService.save(category);
        return R.success(null,"新增分类成功了");
    }

    /**
     * 分类分页查询
     */
    @GetMapping("/page")
    public R<PageResult> findPage(@RequestParam(value = "page", defaultValue = "1") Long page,
                                  @RequestParam(value = "pageSize", defaultValue = "10") Long pageSize) {
        PageResult<Category> pageResult = categoryService.findPage(page,pageSize);
        return R.success(pageResult);
    }
    /**
     * 分类删除
     */
    @DeleteMapping
    public R<String> delete(Long id){
        categoryService.delete(id);
        return R.success(null,"分类删除成功了");
    }


    /**
     * 分类更新
     */
    @PutMapping
    public R<String> update(@RequestBody Category category){
        categoryService.update(category);
        //return R.success(null,"分类更新成功了");
        return R.success("分类更新成功了");
    }

    /**
     * 分类列表查询
     */
    @GetMapping("/list")
    public R<List<Category>> list(Category category){
        return R.success(categoryService.list(category));
    }
}
