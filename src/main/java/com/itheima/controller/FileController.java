package com.itheima.controller;

import com.itheima.common.R;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.UUID;

/**
 * 文件上传
 */
@RestController
@RequestMapping("/common")
@Slf4j
public class FileController {

    //定义文件上传目录
    @Value("${reggie.file-path}")
    private String basePath;

    /**
     * 文件上传
     * springmvc中提供MultipartFile对象 名称 必须要跟 页面文件名称一致
     */
    @PostMapping("/upload")
    public R<String> upload(MultipartFile file){
        //获取原始文件名称
        log.debug("***upload***"+file.getOriginalFilename());
        //1获取原始文件名
        String originalFilename = file.getOriginalFilename(); //1.jpg
        //文件名后缀
        String suffix = originalFilename.substring(originalFilename.lastIndexOf("."));
        //生成的唯一的文件名称
        String uuid = UUID.randomUUID().toString();
        //2进行文件名修改 生成唯一文件名
        String newFileName = uuid + suffix;
        //3创建文件对象 看文件目录是否存在，不存在创建
        File myFile = new File(basePath);
        if(!myFile.exists()){
            myFile.mkdirs();
        }
        //4存在 直接将文件存入此目录中 C:\working\reggieimgs\1.jpg
        try {
            //将上传的file文件 存入C:\working\reggieimgs\1.jpg目录下
            file.transferTo(new File(basePath+newFileName));
        } catch (IOException e) {
            log.debug(e.getMessage());
            return R.error("文件上传失败了");
        }
        //返回的时候 必须返回文件名称 前端根据此名称进行回显
        return R.success(newFileName,"文件上传成功了");
    }


    /**
     * 文件下载
     * String name:需要下载文件名
     * response:将文件以输出流形式下载到浏览器进行回显
     */
    @GetMapping("/download")
    public void download(String name, HttpServletResponse response) throws IOException {
        //从 C:\working\reggieimgs\1.jpg目录下找name这个文件
        FileInputStream inputStream = new FileInputStream(basePath+name);//输入流
        //定义输出流
        ServletOutputStream outputStream = response.getOutputStream(); //输出流
        //将inputStream输入流进行读取 写入outputStream输出流中
        IOUtils.copy(inputStream,outputStream);
        outputStream.close();
        inputStream.close();
        log.debug("**文件下载成功了"+basePath+name+"**");
    }
}
