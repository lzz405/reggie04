package com.itheima.controller;

import com.itheima.common.PageResult;
import com.itheima.common.R;
import com.itheima.dto.DishDto;
import com.itheima.entity.Dish;
import com.itheima.entity.Employee;
import com.itheima.service.DishService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 菜品管理控制层
 * ctrl+b
 */
@RestController
@RequestMapping("/dish")
public class DishController {

    @Autowired
    private DishService dishService;

    /**
     * 新增菜品
     * DishDto：包含菜品+菜品口味数据（前后端保持一致）
     */
    @PostMapping
    public R<String> save(@RequestBody DishDto dishDto){
        dishService.saveWithFlavor(dishDto);
        return R.success("新增菜品成功了");
    }

    /**
     * 菜品分页查询
     */
    @GetMapping("/page")
    public R<PageResult> findPage(@RequestParam(value = "page", defaultValue = "1") Long page,
                                  @RequestParam(value = "pageSize", defaultValue = "10") Long pageSize, String name) {
        PageResult<DishDto> pageResult = dishService.findPage(page,pageSize,name);
        return R.success(pageResult);
    }

    /**
     * 根据菜品主键id查询菜品数据（菜品表+菜品口味表数据）
     */
    @GetMapping("/{id}")
    public R<DishDto> findById(@PathVariable("id") Long id){
        return R.success(dishService.findById(id));
    }

    /**
     * 修改菜品
     */
    @PutMapping
    public R<String> update(@RequestBody DishDto dishDto){
        dishService.update(dishDto);
        return R.success("修改菜品信息成功了");
    }

    /**
     * 根据菜品分类id查询菜品列表数据
     */
    /*@GetMapping("/list")
    public R<List<Dish>> list(Dish dish){
        List<Dish> dishList = dishService.list(dish);
        return R.success(dishList);
    }*/
    @GetMapping("/list")
    public R<List<DishDto>> list(Dish dish){
        List<DishDto> dishDtoList = dishService.list(dish);
        return R.success(dishDtoList);
    }



}
