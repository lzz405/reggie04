package com.itheima.controller;

import com.itheima.common.PageResult;
import com.itheima.common.R;
import com.itheima.dto.DishDto;
import com.itheima.dto.SetmealDto;
import com.itheima.entity.Setmeal;
import com.itheima.service.SetmealService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 套餐管理控制层
 */
@RestController
@RequestMapping("/setmeal")
@Slf4j
public class SetmealController {

    @Autowired
    private SetmealService setmealService;

    /**
     * 新增套餐
     */
    @PostMapping
    public R<String> save(@RequestBody SetmealDto setmealDto){
        setmealService.save(setmealDto);
        return R.success("新增套餐成功了");
    }
    /**
     * 套餐分页查询
     */
    @GetMapping("/page")
    public R<PageResult> findPage(@RequestParam(value = "page", defaultValue = "1") Long page,
                                  @RequestParam(value = "pageSize", defaultValue = "10") Long pageSize, String name) {
        PageResult<SetmealDto> pageResult = setmealService.findPage(page,pageSize,name);
        return R.success(pageResult);
    }

    /**
     * 套餐删除
     * @RequestParam List<Long> ids:接收集合方式  一定要加上@RequestParam
     */
    @DeleteMapping
    public R<String> deleteByIds(@RequestParam List<Long> ids){
        setmealService.deleteByIds(ids);
        return R.success("套餐删除成功了");
    }

    /**
     * 查询套餐列表数据
     */
    @GetMapping("/list")
    public R<List<Setmeal>> list(Setmeal setmeal){
        List<Setmeal> setmealList = setmealService.list(setmeal);
        return R.success(setmealList);
    }
}
