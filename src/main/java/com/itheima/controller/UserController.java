package com.itheima.controller;

import com.itheima.common.R;
import com.itheima.entity.User;
import com.itheima.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * 用户控制层
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * 手机号码发送验证码
     *  HttpSession httpSession：发送验证码成功后 将验证码存入session
     */
    @GetMapping("/sendMsg")
    public R<Integer> sendMsg(String phone, HttpSession httpSession) throws Exception {
        Integer code = userService.sendMsg(phone,httpSession);
        return R.success(code,"发送验证码成功了");
    }

    /**
     * 登录校验
     * 如何接收页面参数？
     * 方式一：自定义类 封装两个参数 phone code
     * 方式二：Map接收  springmvc页面数据进行转换map
     */
    @PostMapping("/login")
    public R<String> login(@RequestBody Map<String,String> params,HttpSession httpSession){
        String phone = params.get("phone");
        String code = params.get("code");
        userService.login(phone,code,httpSession);
        return R.success("登录成功了");
    }

}
