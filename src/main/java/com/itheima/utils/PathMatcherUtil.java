package com.itheima.utils;

import org.springframework.util.AntPathMatcher;

/**
 * 路径匹配器工具方法
 */
public class PathMatcherUtil {
    public static final AntPathMatcher PATH_MATCHER = new AntPathMatcher();

    /**
     * urls：需要放行的资源路径
     * requestURI：请求的uri  例如：/backend/styles/login.css（直接放行）  /backend/index.html（看是否登录决定是否放行）
     * @param urls
     * @param requestURI
     * @return
     */
    public static boolean check(String[] urls,String requestURI){
        for (String url : urls) {
            boolean match = PATH_MATCHER.match(url, requestURI);
            if(match){
                return true;
            }
        }
        return false;
    }
}
