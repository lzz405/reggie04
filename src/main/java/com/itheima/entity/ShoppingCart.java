package com.itheima.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 购物车
 */
@Data
public class ShoppingCart implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    //名称（菜品名称 或 套餐名称）
    private String name;

    //用户id
    private Long userId;

    //菜品id  加入购物车  dishId 或  setmealId 一定只会有一个字段有值
    private Long dishId;

    //套餐id
    private Long setmealId;

    //口味 菜品的口味
    private String dishFlavor;

    //数量  同一个菜品 或  套餐 数量
    private Integer number;

    //金额
    private BigDecimal amount;

    //图片
    private String image;

    //方式一：购物车表 添加4个字段
    //方式二：手动设置下时间
    //@TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;
}
