package com.itheima.service;

import com.itheima.common.PageResult;
import com.itheima.dto.DishDto;
import com.itheima.entity.Dish;

import java.util.List;

/**
 * 菜品业务层接口
 */
public interface DishService {
    /**
     * 新增菜品
     */
    void saveWithFlavor(DishDto dishDto);

    /**
     * 菜品分页查询
     */
    PageResult<DishDto> findPage(Long page, Long pageSize, String name);

    /**
     * 根据菜品主键id查询菜品数据（菜品表+菜品口味表数据）
     */
    DishDto findById(Long id);

    /**
     * 修改菜品
     */
    void update(DishDto dishDto);

    /**
     * 根据菜品分类id查询菜品列表数据
     */
    //List<Dish> list(Dish dish);
    List<DishDto> list(Dish dish);
}
