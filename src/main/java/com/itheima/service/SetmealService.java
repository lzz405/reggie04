package com.itheima.service;

import com.itheima.common.PageResult;
import com.itheima.dto.SetmealDto;
import com.itheima.entity.Setmeal;

import java.util.List;

/**
 * 套餐业务层接口
 */
public interface SetmealService {
    /**
     * 新增套餐
     */
    void save(SetmealDto setmealDto);

    /**
     * 套餐分页查询
     */
    PageResult<SetmealDto> findPage(Long page, Long pageSize, String name);

    /**
     * 套餐删除
     */
    void deleteByIds(List<Long> ids);

    /**
     * 查询套餐列表数据
     */
    List<Setmeal> list(Setmeal setmeal);
}
