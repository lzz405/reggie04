package com.itheima.service;

import com.itheima.entity.DishFlavor;

import java.util.List;

/**
 * 菜品口味业务层接口
 */
public interface DishFlavorService {
    /**
     * 查询口味表
     * @param flavor
     */
    void insert(DishFlavor flavor);

    /**
     * 根据菜品id查询菜品口味表 返回集合
     */
    List<DishFlavor> findById(Long dishId);

    /**
     * 根据菜品id删除菜品口味表数据
     */
    void deleteById(Long dishId);
}
