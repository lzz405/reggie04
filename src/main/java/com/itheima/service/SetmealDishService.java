package com.itheima.service;

import com.itheima.entity.SetmealDish;

import java.util.List;

/**
 * 套餐菜品业务层接口
 */
public interface SetmealDishService {
    /**
     * 套餐菜品保存
     * @param setmealDish
     */
    void insert(SetmealDish setmealDish);

    /**
     * 删除套餐菜品表关联数据
     * @param ids
     */
    void deleteBySetmealIds(List<Long> ids);
}
