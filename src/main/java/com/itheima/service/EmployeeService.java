package com.itheima.service;

import com.itheima.common.PageResult;
import com.itheima.common.R;
import com.itheima.entity.Employee;

import javax.servlet.http.HttpSession;

/**
 * 员工业务处理层接口
 */
public interface EmployeeService {
    /**
     * 后台登录功能
     */
    R<Employee> login(Employee employee, HttpSession httpSession);

    /**
     * 退出
     */
    void logout(HttpSession httpSession);

    /**
     * 新增员工
     * @param employee
     * @param id
     * @return
     */
    int save(Employee employee, Long id);

    /**
     * 员工分页
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    PageResult<Employee> findPage(Long page, Long pageSize, String name);

    /**
     * 更新员工（不仅限于状态更新 员工启用 禁用）
     */
    int update(Employee employee, Long id);

    /**
     * 根据员工id查询员工信息
     */
    Employee findById(Long id);
}
