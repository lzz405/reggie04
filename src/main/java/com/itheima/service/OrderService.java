package com.itheima.service;

import com.itheima.entity.Orders;

import javax.servlet.http.HttpSession;

/**
 * 订单业务层接口
 */
public interface OrderService {
    /**
     * 下单
     */
    void save(Orders orders, HttpSession session);
}
