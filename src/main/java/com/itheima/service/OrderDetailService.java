package com.itheima.service;

/**
 * 订单详情业务层接口
 */
public interface OrderDetailService {
    /**
     * 根据当前用户id查询当前用户购物列表数据
     * 将购物车列表数据copy到订单详情表中进行保存
     * @param ordersId
     */
    void insert(Long ordersId);
}
