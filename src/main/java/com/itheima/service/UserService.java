package com.itheima.service;

import javax.servlet.http.HttpSession;

/**
 * 用户业务层接口
 */
public interface UserService {
    /**
     * 给手机号发送验证码
     * @param phone
     * @param httpSession
     * @return
     */
    Integer sendMsg(String phone, HttpSession httpSession) throws Exception;

    /**
     * 登录校验
     * @param phone
     * @param code
     */
    void login(String phone, String code,HttpSession httpSession);
}
