package com.itheima.service;

import com.itheima.common.PageResult;
import com.itheima.entity.Category;

import java.util.List;

/**
 * 分类业务层接口
 */
public interface CategoryService {
    /**
     * 新增分类（菜品、套餐）
     */
    void save(Category category);

    /**
     * 分类分页查询
     */
    PageResult<Category> findPage(Long page, Long pageSize);

    /**
     * 分类删除
     */
    void delete(Long id);

    /**
     * 分类更新
     */
    void update(Category category);

    /**
     * 分类列表查询
     */
    List<Category> list(Category category);

    /**
     * 根据分类id查询分类对象
     * @param categoryId
     * @return
     */
    Category findById(Long categoryId);
}
