package com.itheima.service;

import com.itheima.entity.ShoppingCart;

import java.util.List;

/**
 * 购物车业务层接口
 */
public interface ShoppingCartService {
    /**
     * 添加购物车
     */
    ShoppingCart save(ShoppingCart shoppingCart);

    /**
     * 查询购物车
     */
    List<ShoppingCart> list();
    /**
     * 清空购物车
     */
    void clean();
}
