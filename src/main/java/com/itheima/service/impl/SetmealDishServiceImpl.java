package com.itheima.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.itheima.entity.SetmealDish;
import com.itheima.mapper.SetmealDishMapper;
import com.itheima.service.SetmealDishService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 套餐菜品实现类
 */
@Service
@Transactional
public class SetmealDishServiceImpl implements SetmealDishService {
    @Autowired
    private SetmealDishMapper setmealDishMapper;

    /**
     * 套餐菜品保存
     *
     * @param setmealDish
     */
    @Override
    public void insert(SetmealDish setmealDish) {
        setmealDishMapper.insert(setmealDish);
    }

    /**
     * 删除套餐菜品表关联数据
     *
     * @param setmealIds
     */
    @Override
    public void deleteBySetmealIds(List<Long> setmealIds) {
        //方式一：delete
        for (Long setmealId : setmealIds) {
            LambdaQueryWrapper<SetmealDish> lambdaQueryWrapper = new LambdaQueryWrapper<>();
            lambdaQueryWrapper.eq(SetmealDish::getSetmealId,setmealId);//套餐id
            setmealDishMapper.delete(lambdaQueryWrapper);
        }
    }
}
