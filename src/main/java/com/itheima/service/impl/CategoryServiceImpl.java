package com.itheima.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.common.PageResult;
import com.itheima.entity.Category;
import com.itheima.entity.Dish;
import com.itheima.entity.Setmeal;
import com.itheima.exception.BusinessException;
import com.itheima.mapper.CategoryMapper;
import com.itheima.mapper.DishMapper;
import com.itheima.mapper.SetmealMapper;
import com.itheima.service.CategoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 分类业务层实现类
 */
@Service
@Transactional
@Slf4j
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    private CategoryMapper categoryMapper;

    @Autowired
    private DishMapper dishMapper;

    @Autowired
    private SetmealMapper setmealMapper;

    /**
     * 新增分类（菜品、套餐）
     *
     * @param category
     */
    @Override
    public void save(Category category) {
        //1 根据分类名称查询分类表中有没有分类
        LambdaQueryWrapper<Category> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(Category::getName, category.getName());//套餐分类 或 菜品分类名称
        Integer count = categoryMapper.selectCount(lambdaQueryWrapper);
        if (count > 0) {
            throw new BusinessException("新增分类[" + category.getName() + "]已经存在了");
        }
        //2 调用持久层新增 套餐分类 或 菜品分类
        int row = categoryMapper.insert(category);
        //3 判断影响行数 == 0 抛出异常 新增分类失败
        if (row == 0) {
            throw new BusinessException("新增分类失败了");
        }
    }

    /**
     * 分类分页查询
     *
     * @param page
     * @param pageSize
     */
    @Override
    public PageResult<Category> findPage(Long page, Long pageSize) {
        // 2.1 new Page(page,pageSize)
        Page pp = new Page(page, pageSize);
        //2.2 selectPage 进行分页
        LambdaQueryWrapper<Category> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.orderByAsc(Category::getSort);//升序 从小到大
        categoryMapper.selectPage(pp, lambdaQueryWrapper);
        //2.3 将page对象中total records 封装到PageResult对象中
        log.debug("员工分页总记录数{}，当前页面数据{}", pp.getTotal(), pp.getRecords());
        return new PageResult<>(pp.getTotal(), pp.getRecords());
    }

    /**
     * 分类删除
     *
     * @param id
     */
    @Override
    public void delete(Long id) {
        //1根据分类id(菜品、套餐)到菜品表dish查询记录是否存在 （存在就说明不能删除）
        LambdaQueryWrapper<Dish> dishQueryWrapper = new LambdaQueryWrapper<>();
        dishQueryWrapper.eq(Dish::getCategoryId, id);
        Integer dishCount = dishMapper.selectCount(dishQueryWrapper);
        if (dishCount > 0) {
            throw new BusinessException("当前分类已经被菜品关联，不能删除");
        }
        //2根据分类id(菜品、套餐)到套餐表setmeal查询记录是否存在 （存在就说明不能删除）
        LambdaQueryWrapper<Setmeal> setmealQueryWrapper = new LambdaQueryWrapper<>();
        setmealQueryWrapper.eq(Setmeal::getCategoryId, id);
        Integer setmealCount = setmealMapper.selectCount(setmealQueryWrapper);
        if (setmealCount > 0) {
            throw new BusinessException("当前分类已经被套餐关联，不能删除");
        }
        //3根据分类id到category进行删除
        categoryMapper.deleteById(id);
    }

    /**
     * 分类更新
     *
     * @param category
     */
    @Override
    public void update(Category category) {
        //1根据分类名称 查询分类表中分类是否存在 并 排除当前分类id
        LambdaQueryWrapper<Category> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Category::getName, category.getName());//修改后分类名称
        queryWrapper.ne(Category::getId, category.getId());//排除当前分类id
        Integer count = categoryMapper.selectCount(queryWrapper);
        if (count > 0) {
            throw new BusinessException("修改分类[" + category.getName() + "]已经存在了");
        }
        //2更新分类
        categoryMapper.updateById(category);
    }

    /**
     * 分类列表查询
     *
     * @param category
     */
    @Override
    public List<Category> list(Category category) {
        //获取type类型
        Integer type = category.getType();
        LambdaQueryWrapper<Category> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        //条件 type =1  or  type = 2   没有条件查询全部
        lambdaQueryWrapper.eq(type != null,Category::getType,type);
        lambdaQueryWrapper.orderByAsc(Category::getSort);//从小到大 菜品分类进行排序
        return categoryMapper.selectList(lambdaQueryWrapper);
    }

    /**
     * 根据分类id查询分类对象
     *
     * @param categoryId
     * @return
     */
    @Override
    public Category findById(Long categoryId) {
        return categoryMapper.selectById(categoryId);
    }
}
