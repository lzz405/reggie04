package com.itheima.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.itheima.entity.DishFlavor;
import com.itheima.mapper.DishFlavorMapper;
import com.itheima.service.DishFlavorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 菜品口味业务层接口实现类
 */
@Service
@Transactional
public class DishFlavorServiceImpl implements DishFlavorService {

    @Autowired
    private DishFlavorMapper dishFlavorMapper;

    /**
     * 查询口味表
     *
     * @param flavor
     */
    @Override
    public void insert(DishFlavor flavor) {
        dishFlavorMapper.insert(flavor);
    }

    /**
     * 根据菜品id查询菜品口味表 返回集合
     *
     * @param dishId
     */
    @Override
    public List<DishFlavor> findById(Long dishId) {
        LambdaQueryWrapper<DishFlavor> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(DishFlavor::getDishId,dishId);//根据菜品id查询菜品口味集合数据
        return dishFlavorMapper.selectList(lambdaQueryWrapper);
    }

    /**
     * 根据菜品id删除菜品口味表数据
     *
     * @param dishId
     */
    @Override
    public void deleteById(Long dishId) {
        LambdaQueryWrapper<DishFlavor> dishFlavorLambdaQueryWrapper = new LambdaQueryWrapper<>();
        dishFlavorLambdaQueryWrapper.eq(DishFlavor::getDishId,dishId);//菜品id作为条件删除 菜品关联的所有口味数据
        dishFlavorMapper.delete(dishFlavorLambdaQueryWrapper);
    }


}
