package com.itheima.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.itheima.entity.ShoppingCart;
import com.itheima.mapper.ShoppingCartMapper;
import com.itheima.service.ShoppingCartService;
import com.itheima.utils.BaseContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 购物车业务层实现类
 */
@Service
@Transactional
@Slf4j
public class ShoppingCartServiceImpl implements ShoppingCartService {

    @Autowired
    private ShoppingCartMapper shoppingCartMapper;

    /**
     * 添加购物车
     *
     * @param shoppingCart
     */
    @Override
    public ShoppingCart save(ShoppingCart shoppingCart) {
        Long currentUserId = BaseContext.getCurrentId();//当前用户id
        Long dishId = shoppingCart.getDishId();//菜品id
        Long setmealId = shoppingCart.getSetmealId();//套餐id
        //1判断dishId是否为空 如果不为空
        //根据dishId 和 userId 作为条件查询购物车表 记录是否存在
        ShoppingCart sc = new ShoppingCart();
        if(dishId != null){
            LambdaQueryWrapper<ShoppingCart> shoppingCartLambdaQueryWrapper = new LambdaQueryWrapper<>();
            shoppingCartLambdaQueryWrapper.eq(ShoppingCart::getDishId,dishId);//菜品id
            shoppingCartLambdaQueryWrapper.eq(ShoppingCart::getUserId,currentUserId);//当前用户id
            sc = shoppingCartMapper.selectOne(shoppingCartLambdaQueryWrapper);
        }else {
            //2判断dishId是否为空 如果为空
            //根据setmealId 和 userId 作为条件查询购物车表 记录是否存在
            LambdaQueryWrapper<ShoppingCart> shoppingCartLambdaQueryWrapper = new LambdaQueryWrapper<>();
            shoppingCartLambdaQueryWrapper.eq(ShoppingCart::getSetmealId,setmealId);//套餐id
            shoppingCartLambdaQueryWrapper.eq(ShoppingCart::getUserId,currentUserId);//当前用户id
            sc = shoppingCartMapper.selectOne(shoppingCartLambdaQueryWrapper);
        }
        //3购物车记录不存在菜品或套餐数据 添加数据到购物车表中
        if(sc == null) {
            shoppingCart.setUserId(currentUserId);//某个用户的购物车
            shoppingCart.setNumber(1);//第一次添加菜品或套餐 设置数量为1
            shoppingCart.setCreateTime(LocalDateTime.now());
            shoppingCartMapper.insert(shoppingCart);
            return shoppingCart;
        }else{
            //4.购物车记录存在菜品或套餐数据 修改购物车表中数量number
            sc.setNumber(sc.getNumber()+1);
            shoppingCartMapper.updateById(sc);//根据购物车表主键id更新number数量+1
            return sc;
        }

    }

    /**
     * 查询购物车
     */
    @Override
    public List<ShoppingCart> list() {
        LambdaQueryWrapper<ShoppingCart> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(ShoppingCart::getUserId,BaseContext.getCurrentId());//获取当前用户的购物车数据
        lambdaQueryWrapper.orderByDesc(ShoppingCart::getCreateTime); //按加入购物车时间 将最新添加数据放最前面
        return shoppingCartMapper.selectList(lambdaQueryWrapper);
    }

    /**
     * 清空购物车
     */
    @Override
    public void clean() {
        LambdaQueryWrapper<ShoppingCart> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(ShoppingCart::getUserId,BaseContext.getCurrentId());//获取当前用户的购物车数据
        shoppingCartMapper.delete(lambdaQueryWrapper);
    }
}
