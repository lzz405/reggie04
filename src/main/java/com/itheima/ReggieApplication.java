package com.itheima;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

/**
 * 瑞吉外卖启动类
 * 创建接口代理对象：
 * 方式一：持久层接口继承BaseMapper
 * 方式二：启动类上使用@MapperScan("com.itheima.mapper")
 */
@SpringBootApplication
@Slf4j //负责输出日志注解
//@MapperScan("com.itheima.mapper")
@ServletComponentScan //注意：@ServletComponentScan会自动扫描项目中(当前包及其子包下)的@WebServlet , @WebFilter , @WebListener 注解, 自动注册Servlet的相关组件
public class ReggieApplication {

    public static void main(String[] args) {
        SpringApplication.run(ReggieApplication.class,args);
        log.debug("*************************瑞吉外卖启动成功了 哈哈*************************");
    }
}
