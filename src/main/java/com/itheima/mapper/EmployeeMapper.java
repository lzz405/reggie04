package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.entity.Employee;
import org.apache.ibatis.annotations.Mapper;

/**
 * 员工持久层接口
 */
@Mapper
public interface EmployeeMapper extends BaseMapper<Employee> {
}
