package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.entity.SetmealDish;
import org.apache.ibatis.annotations.Mapper;

/**
 * 套餐菜品持久层接口（1个套餐对应多个菜品）
 */
@Mapper
public interface SetmealDishMapper extends BaseMapper<SetmealDish> {
}
