package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.entity.Orders;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单持久层接口
 */
@Mapper
public interface OrderMapper extends BaseMapper<Orders> {
}
