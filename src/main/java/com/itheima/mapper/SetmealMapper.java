package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.entity.Setmeal;
import org.apache.ibatis.annotations.Mapper;

/**
 * 套餐持久层接口
 */
@Mapper
public interface SetmealMapper extends BaseMapper<Setmeal> {
}
