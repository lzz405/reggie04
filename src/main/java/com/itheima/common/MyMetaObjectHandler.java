package com.itheima.common;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.itheima.utils.BaseContext;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * 自定义自动填充实现类
 */
@Component
@Slf4j
public class MyMetaObjectHandler implements MetaObjectHandler {
    /**
     * 插入元对象字段填充（用于插入时对公共字段的填充）
     *
     * @param metaObject 元对象
     */
    @Override
    public void insertFill(MetaObject metaObject) {
        log.debug("insertFill**自动填充**");
        //填充创建时间
        metaObject.setValue("createTime", LocalDateTime.now());
        //填充修改时间
        metaObject.setValue("updateTime", LocalDateTime.now());
        //填充创建用户id 暂时写死 等下再改造
        //从本地线程对象中获取用户id
        metaObject.setValue("createUser", BaseContext.getCurrentId());
        //填充更新用户id
        metaObject.setValue("updateUser", BaseContext.getCurrentId());
        long id = Thread.currentThread().getId();
        log.debug("**MyMetaObjectHandler**线程id{}**",id);
    }

    /**
     * 更新元对象字段填充（用于更新时对公共字段的填充）
     *
     * @param metaObject 元对象
     */
    @Override
    public void updateFill(MetaObject metaObject) {

        log.debug("updateFill**自动填充**");
        //填充修改时间
        metaObject.setValue("updateTime", LocalDateTime.now());
        //填充更新用户id
        metaObject.setValue("updateUser", BaseContext.getCurrentId());
        long id = Thread.currentThread().getId();
        log.debug("**MyMetaObjectHandler**线程id{}**",id);
    }
}
