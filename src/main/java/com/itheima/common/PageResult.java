package com.itheima.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

/**
 * 自定义分页对象
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageResult<T> implements Serializable {
    private long total = 0l;//总记录数
    //Collections.emptyList()  跟  new ArrayList<>()区别 ：前者不能进行添加删除操作 可以正常查询
    private List<T>  records = Collections.emptyList();
    //private List<T>  records = new ArrayList<>();
}
